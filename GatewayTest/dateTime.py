import datetime
import sys

timeStamp = datetime.datetime.now()
sys.stdout.write(timeStamp.strftime("%Y-%m-%d %H:%M:%S"))

